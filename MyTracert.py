import subprocess


def nslookup(domain= "www.facebook.com"):
    process = subprocess.Popen(["nslookup", domain], stdout=subprocess.PIPE)
    output = process.communicate()[0].split('\n')
    mac = False
    ip_arr = []
    for data in output:
        if 'Address' in data:
            ip_arr.append(data.replace('Address:  ','').replace('\r',''))
            mac = True
        if '\t  ' in data:
            ip_arr.append(data.replace('\t  ', '').replace('\r',''))
            if mac:
                ip_arr.pop(0)
                mac = False
    ip_arr.pop(0)

    print "ip list: ",ip_arr
    return ip_arr


def trace(domain, ip_list):
    ip = None
    ttl =1

    while ip not in ip_list:
        process = subprocess.Popen(["ping", "-n", "1", "-i", str(ttl), domain], stdout=subprocess.PIPE)
        output = process.communicate()[0].split('\n')

        for row in output:
            if "Reply from " in row:
                ip = row.replace('Reply from ','')
                ip = ip[0:ip.find(':')]
                print str(ttl)+')', ip

        ttl+=1

def main():
    domain = raw_input("please enter a domain: ")
    ip = nslookup(domain)
    trace(domain,ip)

if __name__ == "__main__":
    main()